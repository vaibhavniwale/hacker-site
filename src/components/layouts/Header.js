import React,{ Component } from 'react';
import hnimg from '../images/hn.png'
import searchImg from '../images/search.png'

class Header extends Component {
  render(){
    return (
      <div style ={headerStyle}>
        <section style={hackernews}>
          <img src={hnimg} alt='logo' style={hnStyle}/>
          <h1>Hacker News</h1>
        </section>
        <section style={search}>
          <img src={searchImg} alt='search' style={searchImgStyle}/>
          <input  style={searchStyle} placeholder='Search here'/>
        </section>
      </div>
    )
  }
}

const search = {
  border: 'solid 1px',
  borderRadius: '10px',
  height: '26px',
  flexDirection: 'row',
  justifyContent:'center',
  alignItems: 'center',
  marginLeft: '5px',
  paddingLeft:'5px',
  paddingRight: '5px',
  backgroundColor: 'white',

}

const hackernews = {
  display: 'flex',
  alignItems:'center'
}

const searchImgStyle ={
  marginLeft:'5px',
  height: '12px',
  width: '12px',
}

const hnStyle = {
  height: '30px',
  width: '30px'
}

const headerStyle = {
  height: '60px',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-around',
  alignItems: 'center',
  padding:'15px',
  backgroundColor:'Orange',
  color: 'white'
}

const searchStyle ={
  marginLeft:'5px',
  border:'none',
  textSize:'10px',
}

export default Header;

