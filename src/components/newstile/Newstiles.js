import React,{ Component } from 'react';
import Newtile from './Newtile'

class Newstiles extends Component {
  render(){
    return this.props.newstiles.map((element)=>(
      <Newtile key={element.objectID} newtile={element}/>
    ))
  }
}


export default Newstiles;
