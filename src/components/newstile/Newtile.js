import React,{ Component } from 'react';
import linkimg from '../images/link.png'
import view from '../images/like.png'
import time from '../images/time.png'
import comment from '../images/comment.png'
import profileimg from '../images/icon.png'

class Newtile extends Component {
  render(){
    return (
        <div className='story' style={storyStyle}>
          <p style={cardheader}>
          <p >{ this.props.newtile.title }</p>
          <a href={ this.props.newtile.url }>
              <img src={linkimg} alt='link' style={url}/>
          </a>
          </p>
          <p style={cardfooter}>
           <img src={profileimg} alt='link' style={url}/>  { this.props.newtile.author }&nbsp;&nbsp;|&nbsp;&nbsp;
          <img src={time} alt='time' style={url}/> { secondToDate(this.props.newtile.created_at_i) }&nbsp;&nbsp;|&nbsp;&nbsp;
          <img src={view} alt='view' style={url}/> { this.props.newtile.points }&nbsp;&nbsp;|&nbsp;&nbsp;
          <img src={comment} alt='comment' style={url}/> { this.props.newtile.num_comments }
          </p>
          <p style={texstyle} dangerouslySetInnerHTML={{ __html: this.props.newtile.story_text }}></p>
          
        </div>
      )
  }
}

const texstyle={
  fontSize: '12px'
}

const cardfooter ={
  display: 'flex',
  fontSize:'10.6667px',
  alignItems:'center'
}

const cardheader ={
  display: 'flex',
  fontWeight: 'bold',
  fontSize:'14px'
}


const url = {
  height:'7px',
  width:'8px'
}

const storyStyle ={
  backgroundColor: "#F0F0F0",
  borderBottom: 'solid 1px',
  display:'flex',
  padding:'10px',
  flexDirection:'column',
  margin: '0px 10px',
  justifyContent:'center'
}


function secondToDate(unix_timestamp){
  let currentTime = Date.now()
  let articleTime = unix_timestamp*1000;
  let timeDifference = currentTime-articleTime
  let timediff = new Date(timeDifference)
  if(timeDifference > 2678400000){
    currentTime = new Date(Date.now())
    articleTime = new Date(unix_timestamp*1000)
    return `${currentTime.getFullYear()-articleTime.getFullYear()}y`
  }else if(timeDifference > 2678400000){
      return `${timediff.getMonth()}m`
  }else if(timeDifference > 86400000){
        return `${timediff.getDate()}d`
  }else if(timeDifference > 3600000){
    return `${timediff.getHours()}h`
  }else if(timeDifference > 60000){
    return `${timediff.getMinutes()}m`
  }else{
    return `Few seconds ago`
  }
}

export default Newtile;

