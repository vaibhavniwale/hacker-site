import React,{ Component } from 'react';
import Newstiles from './components/newstile/Newstiles'
import Header from './components/layouts/Header'

import './App.css';

class App extends Component {
  state = {
    trending: [],
    lasthour:[],
    lastday:[],
    lastweek:[],
    lastmonth:[],
    older:[]
  }
   componentDidMount(){
    let dateholder = Date.now()/1000
    fetch('https://hn.algolia.com/api/v1/search?tags=story')
          .then((res)=>{
            return res.json()
          })
          .then((resp)=>{
          console.log(resp.hits)
          this.setState({trending:resp.hits})
        })
        .catch((err)=>{
          console.log(err)
        })
    fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i%3E1160418110,created_at_i%3C${dateholder}`)
        .then((res)=>{
          return res.json()
        }
      ).then((resp)=>{
        console.log(resp.hits)
        this.setState({lasthour:resp.hits})
      })
      .catch((err)=>{
        console.log(err)
      }) 
    fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i%3E1160418110,created_at_i%3C${dateholder-3600}`)
      .then((res)=>{
        return res.json()
      }
    ).then((resp)=>{
      console.log(resp.hits)
      this.setState({lastday:resp.hits})
    })
    .catch((err)=>{
      console.log(err)
    })
  fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i%3E1160418110,created_at_i%3C${dateholder-86400}`)
        .then((res)=>{
          return res.json()
        }
      ).then((resp)=>{
        console.log(resp.hits)
        this.setState({lastweek:resp.hits})
      })  
      .catch((err)=>{
        console.log(err)
      })
  fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i%3E1160418110,created_at_i%3C${dateholder-604800}`)
    .then((res)=>{
      return res.json()
    }
  ).then((resp)=>{
    console.log(resp.hits)
    this.setState({lastmonth:resp.hits})
  }) 
  .catch((err)=>{
    console.log(err)
  })

  fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i%3E1160418110,created_at_i%3C${dateholder-2592000}`)
  .then((res)=>{
    return res.json()
  }
).then((resp)=>{
  console.log(resp.hits)
  this.setState({older:resp.hits})
})
.catch((err)=>{
  console.log(err)
}) 
  }
  render(){
    return (
      <div className="App">
        <Header />
          <Newstiles newstiles={this.state.trending} />
      </div>
    );
  }
}





export default App;
